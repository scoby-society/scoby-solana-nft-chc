import { Fragment, useRef, useState, useEffect } from 'react';
import { useWallet, useConnection } from "@solana/wallet-adapter-react";
import {
  Connection,
  Keypair,
  PublicKey,
  Transaction,
  TransactionInstruction,
  ConfirmOptions,
  LAMPORTS_PER_SOL,
  SystemProgram,
  clusterApiUrl,
  SYSVAR_RENT_PUBKEY,
  SYSVAR_CLOCK_PUBKEY
} from '@solana/web3.js'
import {AccountLayout,MintLayout,TOKEN_PROGRAM_ID,ASSOCIATED_TOKEN_PROGRAM_ID,Token} from "@solana/spl-token";
import useNotify from './notify'
import * as bs58 from 'bs58'
import * as anchor from "@project-serum/anchor";
import { programs } from '@metaplex/js';
import axios from "axios"
import {WalletConnect, WalletDisconnect} from '../wallet'
import { Container, Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { CircularProgress, Card, CardMedia, Grid, CardContent, Typography, BottomNavigation,
				Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper  } from '@mui/material'
import {createMint,createAssociatedTokenAccountInstruction,sendTransactionWithRetry} from './utility'

let wallet : any
let conn = new Connection(clusterApiUrl('mainnet-beta'))
let notify: any

const { metadata: { Metadata } } = programs
//real collection 
const TOKEN_METADATA_PROGRAM_ID = new anchor.web3.PublicKey("metaqbxxUerdq28cj1RbAWkYQm3ybzjb6a8bt518x1s")
const chcProgramId = new PublicKey('C53zanAXBqTdexRy3mPDgA9LbQ6X9X5V58ngJrroCVhU')
const chcPOOL = new PublicKey('H2Q275YxGZud2cUJAodJSEYRzHzQVi2P5j9wSUQvDBUY')
const chcSYMBOL = "CHC"
const chcIdl = require('./chc-nft.json')


// royalty nft
const nfpProgramId = new PublicKey('EJwH4UAsvAbunQtG2srAtULxJh1vGkzVFMeaikBnytiZ')
const nfpPOOL = new PublicKey('GnqYk16oGeR1VWddzDc7AxzNKqhkcZNCTvPp6ZWvKd6v')
const nfpSYMBOL = "RLTY"
const nfpIdl = require('./nfp-nft.json')

// membership nft
const membershipProgramId = new PublicKey('CwwjbR6zspiVgKvLq6ufs5u8TZ3WTKYexaQS6kHPQcku')
const membershipPOOL = new PublicKey('3sPPc4TiATfBTUg9eNmXrq6cGCMye9AN9ZinUMRHQy9g')
const membershipSYMBOL = "SCOBYNYC"
const membershipIdl = require('./membership.json')

// hellbenders nft
const hellbendersProgramId = new PublicKey('CwwjbR6zspiVgKvLq6ufs5u8TZ3WTKYexaQS6kHPQcku')
const hellbendersPOOL = new PublicKey('9nTEDecxRMhbLibVjCGuvFx64T1A21ZCvVJENkbJLLy6')
const hellbendersSYMBOL = "HBMCW"
// const hellbendersIdl = require('./membership.json')

const confirmOption : ConfirmOptions = {commitment : 'finalized',preflightCommitment : 'finalized',skipPreflight : false}

interface Schedule{
	time : string;
	amount : string;
}

let defaultSchedule = {
	time : '', amount : ''
}

interface AlertState {
  open: boolean;
  message: string;
  severity: 'success' | 'info' | 'warning' | 'error' | undefined;
}

export default function Mint(){
	wallet = useWallet()
	notify = useNotify()

	const [chcPool, setPool] = useState<PublicKey>(chcPOOL)
	const [alertState, setAlertState] = useState<AlertState>({open: false,message: '',severity: undefined})
	const [isProcessing, setIsProcessing] = useState(false)
	const [holdingNfts, setHoldingNfts] = useState<any[]>([])
	const [poolData, setPoolData] = useState<any>(null)

	useEffect(()=>{
		getPoolData()
	},[chcPool])

	useEffect(()=>{
		if(poolData != null && wallet.publicKey != null){
			getNfpNftsForOwner(wallet.publicKey, nfpSYMBOL)
		}
	},[wallet.publicKey,poolData])

	const getTokenWallet = async (owner: PublicKey,mint: PublicKey) => {
	  return (
	    await PublicKey.findProgramAddress(
	      [owner.toBuffer(), TOKEN_PROGRAM_ID.toBuffer(), mint.toBuffer()],
	      ASSOCIATED_TOKEN_PROGRAM_ID
	    )
	  )[0];
	}
	const getMetadata = async (mint: PublicKey) => {
	  return (
	    await anchor.web3.PublicKey.findProgramAddress(
	      [
	        Buffer.from("metadata"),
	        TOKEN_METADATA_PROGRAM_ID.toBuffer(),
	        mint.toBuffer(),
	      ],
	      TOKEN_METADATA_PROGRAM_ID
	    )
	  )[0];
	}
	const getEdition = async (mint: PublicKey) => {
	  return (
	    await anchor.web3.PublicKey.findProgramAddress(
	      [
	        Buffer.from("metadata"),
	        TOKEN_METADATA_PROGRAM_ID.toBuffer(),
	        mint.toBuffer(),
	        Buffer.from("edition")
	      ],
	      TOKEN_METADATA_PROGRAM_ID
	    )
	  )[0];
	}
	const getPoolData = async() => {
		try{
			const poolAddress = new PublicKey(chcPOOL)
			const randWallet = new anchor.Wallet(Keypair.generate())
			const provider = new anchor.Provider(conn,randWallet,confirmOption)
			const program = new anchor.Program(chcIdl,chcProgramId,provider)
			const pD = await program.account.pool.fetch(poolAddress)
			setPoolData(pD)
		} catch(err){
			console.log(err)
			setPoolData(null)
		}
	}	

	async function getNfpNftsForOwner(owner : PublicKey,	symbol : string) {
		let allTokens: any[] = []
		const tokenAccounts = await conn.getParsedTokenAccountsByOwner(owner, {programId: TOKEN_PROGRAM_ID},"finalized");
		console.log(tokenAccounts);
		const randWallet = new anchor.Wallet(Keypair.generate())
		const provider = new anchor.Provider(conn,randWallet,confirmOption)
		const nfpProgram = new anchor.Program(nfpIdl,nfpProgramId,provider)
		const membershipProgram = new anchor.Program(membershipIdl,membershipProgramId,provider)
  
		for (let index = 0; index < tokenAccounts.value.length; index++) {
			try{
				const tokenAccount = tokenAccounts.value[index];
				const tokenAmount = tokenAccount.account.data.parsed.info.tokenAmount;

				if (tokenAmount.amount == "1" && tokenAmount.decimals == "0") {
					let nftMint = new PublicKey(tokenAccount.account.data.parsed.info.mint)
					let pda = await getMetadata(nftMint)
					const accountInfo: any = await conn.getParsedAccountInfo(pda);
					let metadata : any = new Metadata(owner.toString(), accountInfo.value)
					
					if (metadata.data.data.symbol == symbol) {
						let [metadataExtended, bump] = await PublicKey.findProgramAddress([nftMint.toBuffer(), nfpPOOL.toBuffer()],nfpProgramId)

						if((await conn.getAccountInfo(metadataExtended)) == null) continue;

						let extendedData = await nfpProgram.account.metadataExtended.fetch(metadataExtended)
						// const { data }: any = await axios.get(metadata.data.data.uri)
						// const entireData = { ...data, id: Number(data.name.replace( /^\D+/g, '').split(' - ')[0])}

						allTokens.push(
							{
								mint : nftMint,
								metadata : pda, 
								tokenAccount :  tokenAccount.pubkey,
								metadataExtended : metadataExtended, 
								extendedData : extendedData,
								data : metadata.data.data,
								// offChainData : entireData, 
							}
						)
					}
				}
			} 
			catch(err) {
				continue;
			}
		}
		allTokens.sort(function(a:any, b: any){
			if(a.extendedData.number < b.extendedData.number) {return -1;}
			if(a.extendedData.number > b.extendedData.number) {return 1;}
			return 0;
		})
		console.log(allTokens)
		setHoldingNfts(allTokens)
		return allTokens
	}

	async function getMembershipNftsForOwner(owner : PublicKey, collection_pool: PublicKey, symbol : string) {
		let allTokens: any[] = []
		const tokenAccounts = await conn.getParsedTokenAccountsByOwner(owner, {programId: TOKEN_PROGRAM_ID},"finalized");
		console.log(tokenAccounts);
		const randWallet = new anchor.Wallet(Keypair.generate())
		const provider = new anchor.Provider(conn,randWallet,confirmOption)
		const membershipProgram = new anchor.Program(membershipIdl,membershipProgramId,provider)
  
		for (let index = 0; index < tokenAccounts.value.length; index++) {
			try{
				const tokenAccount = tokenAccounts.value[index];
				const tokenAmount = tokenAccount.account.data.parsed.info.tokenAmount;

				if (tokenAmount.amount == "1" && tokenAmount.decimals == "0") {
					let nftMint = new PublicKey(tokenAccount.account.data.parsed.info.mint)
					let pda = await getMetadata(nftMint)
					const accountInfo: any = await conn.getParsedAccountInfo(pda);
					let metadata : any = new Metadata(owner.toString(), accountInfo.value)
					
					if (metadata.data.data.symbol == symbol) {
						let [metadataExtended, bump] = await PublicKey.findProgramAddress([nftMint.toBuffer(), collection_pool.toBuffer()],membershipProgramId)

						if((await conn.getAccountInfo(metadataExtended)) == null) continue;

						let extendedData = await membershipProgram.account.metadataExtended.fetch(metadataExtended)
						// const { data }: any = await axios.get(metadata.data.data.uri)
						// const entireData = { ...data, id: Number(data.name.replace( /^\D+/g, '').split(' - ')[0])}

						allTokens.push(
							{
								mint : nftMint,
								metadata : pda, 
								tokenAccount :  tokenAccount.pubkey,
								metadataExtended : metadataExtended, 
								extendedData : extendedData,
								data : metadata.data.data,
								// offChainData : entireData, 
							}
						)
					}
				}
			} 
			catch(err) {
				continue;
			}
		}
		allTokens.sort(function(a:any, b: any){
			if(a.extendedData.number < b.extendedData.number) {return -1;}
			if(a.extendedData.number > b.extendedData.number) {return 1;}
			return 0;
		})
		console.log(allTokens)
		setHoldingNfts(allTokens)
		return allTokens
	}

	const mint = async() =>{
		try{
			const provider = new anchor.Provider(conn, wallet as any, confirmOption)
			const chcProgram = new anchor.Program(chcIdl,chcProgramId,provider)
			const chcPoolData = await chcProgram.account.pool.fetch(chcPool)

			// // nfp
			// const nfpProgram = new anchor.Program(nfpIdl,nfpProgramId,provider)
			// const nfpPoolData = await nfpProgram.account.pool.fetch(nfpPOOL)
			// const nfpConfigData = await nfpProgram.account.config.fetch(nfpPoolData.config)

			let transaction = new Transaction()
			let instructions : TransactionInstruction[] = []
			let signers : Keypair[] = []
			const mintRent = await conn.getMinimumBalanceForRentExemption(MintLayout.span)
			const mintKey = createMint(instructions, wallet.publicKey,mintRent,0,wallet.publicKey,wallet.publicKey,signers)
			const recipientKey = await getTokenWallet(wallet.publicKey, mintKey)
			createAssociatedTokenAccountInstruction(instructions,recipientKey,wallet.publicKey,wallet.publicKey,mintKey)
			instructions.push(Token.createMintToInstruction(TOKEN_PROGRAM_ID,mintKey,recipientKey,wallet.publicKey,[],1))
			instructions.forEach(item=>transaction.add(item))
			
			const metadata = await getMetadata(mintKey)
			const masterEdition = await getEdition(mintKey)
			const [metadataExtended, bump] = await PublicKey.findProgramAddress([mintKey.toBuffer(),chcPool.toBuffer()], chcProgramId)

			// get requirements

			let nfps = await getNfpNftsForOwner(wallet.publicKey, nfpSYMBOL)
			// if(nfps.length==0) throw new Error("You do not have any nfp")
			
			if(nfps.length === 0) 
			{
				nfps = await getMembershipNftsForOwner(wallet.publicKey, membershipPOOL, membershipSYMBOL)
				
			}

			if(nfps.length === 0) 
			{
				nfps = await getMembershipNftsForOwner(wallet.publicKey, hellbendersPOOL, hellbendersSYMBOL)
				
			}
			
			if(nfps.length === 0) throw new Error("You do not have any nfps and memberships")
			
			let oldestNfp = nfps[0]

			console.log("chcpooldata", chcPoolData);

			if(chcPoolData.countMinting == 0){
				console.log("good")
				const creatorScoutResp = await conn.getTokenLargestAccounts(oldestNfp.extendedData.parentNfp,'finalized')
				if(creatorScoutResp==null || creatorScoutResp.value==null || creatorScoutResp.value.length==0) throw new Error("Invalid creator")
				const creatorScoutNftAccount = creatorScoutResp.value[0].address

				transaction.add(chcProgram.instruction.mintRoot(new anchor.BN(bump),{
					accounts : {
						owner : wallet.publicKey,
						pool : chcPOOL,
						config : chcPoolData.config,
						nftMint : mintKey,
						nftAccount : recipientKey,
						metadata : metadata,
						masterEdition : masterEdition,
						metadataExtended : metadataExtended,
						relatedNfpMint : oldestNfp.mint,
						relatedNfpAccount : oldestNfp.tokenAccount,
						creatorScoutNftMint : oldestNfp.extendedData.parentNfp,
						creatorScoutNftAccount : creatorScoutNftAccount,
						scobyWallet : chcPoolData.scobyWallet,
						tokenProgram : TOKEN_PROGRAM_ID,
						tokenMetadataProgram : TOKEN_METADATA_PROGRAM_ID,
						systemProgram : SystemProgram.programId,
						rent : SYSVAR_RENT_PUBKEY,
					}
				}))
			}else{
				
				// creator
				const creatorMint = chcPoolData.rootNft
				console.log(creatorMint.toString());
				const creatorResp = await conn.getTokenLargestAccounts(creatorMint,'finalized')
				console.log("creator response", creatorResp);
				if(creatorResp==null || creatorResp.value==null || creatorResp.value.length==0) throw new Error("Invalid creator")
				const creatorNftAccount = creatorResp.value[0].address
				const creatorInfo = await conn.getAccountInfo(creatorNftAccount,'finalized')
				if(creatorInfo == null) throw new Error('Creator NFT info failed')
				const accountCreatorInfo = AccountLayout.decode(creatorInfo.data)
				if(Number(accountCreatorInfo.amount)==0) throw new Error("Invalid Creator Info")
				const creatorWallet = new PublicKey(accountCreatorInfo.owner)

				// creator scout
				const creatorScoutMint = chcPoolData.creatorScout
				const creatorScoutResp = await conn.getTokenLargestAccounts(creatorScoutMint,'finalized')
				if(creatorScoutResp==null || creatorScoutResp.value==null || creatorScoutResp.value.length==0) throw new Error("Invalid creator Scout")
				const creatorScoutNftAccount = creatorScoutResp.value[0].address
				const creatorScoutInfo = await conn.getAccountInfo(creatorScoutNftAccount,'finalized')
				if(creatorScoutInfo == null) throw new Error('Creator Scout NFT info failed')
				const accountCreatorScoutInfo = AccountLayout.decode(creatorScoutInfo.data)
				if(Number(accountCreatorScoutInfo.amount)==0) throw new Error("Invalid Creator Scout Info")
				const creatorScoutWallet = new PublicKey(accountCreatorScoutInfo.owner)

				// parent nfp
				const parentNfpResp = await conn.getTokenLargestAccounts(oldestNfp.extendedData.parentNfp, 'finalized')
				if(parentNfpResp==null || parentNfpResp.value==null || parentNfpResp.value.length==0) throw new Error("Invalid parent")
				const parentNfpAccount = parentNfpResp.value[0].address
				const parentNfpInfo = await conn.getAccountInfo(parentNfpAccount, 'finalized')
				if(parentNfpInfo == null) throw new Error('Parent NFT info failed');
				const parentNfpAccountInfo = AccountLayout.decode(parentNfpInfo.data)
				if(Number(parentNfpAccountInfo.amount)==0) throw new Error("Invalid Parent info")
				const parentWallet = new PublicKey(parentNfpAccountInfo.owner)

				// let [parentNfpMetadataExtended, parentBump] = await PublicKey.findProgramAddress([oldestNfp.extendedData.relatedNfp.toBuffer(), nfpPOOL.toBuffer()],nfpProgramId)
				

				// grandparent nfp
				const grandParentNfpResp = await conn.getTokenLargestAccounts(oldestNfp.extendedData.grandParentNfp, 'finalized')
				if(grandParentNfpResp==null || grandParentNfpResp.value==null || grandParentNfpResp.value.length==0) throw new Error("Invalid grandParent")
				const grandParentNfpAccount = grandParentNfpResp.value[0].address
				const grandParentNfpInfo = await conn.getAccountInfo(grandParentNfpAccount, 'finalized')
				if(grandParentNfpInfo == null) throw new Error('grandParent NFT info failed');
				const grandParentNfpAccountInfo = AccountLayout.decode(grandParentNfpInfo.data)
				if(Number(grandParentNfpAccountInfo.amount)==0) throw new Error("Invalid grandParent info")
				const grandParentWallet = new PublicKey(grandParentNfpAccountInfo.owner)

				// let [grandParentNfpMetadataExtended, grandParentBump] = await PublicKey.findProgramAddress([oldestNft.extendedData.parentNfp.toBuffer(), nfpPOOL.toBuffer()],nfpProgramId)
				
				console.log("good")
				transaction.add(chcProgram.instruction.mint(new anchor.BN(bump),{
					accounts : {
						owner : wallet.publicKey,
						pool : chcPOOL,
						config : chcPoolData.config,
						nftMint : mintKey,
						nftAccount : recipientKey,
						metadata : metadata,
						masterEdition : masterEdition,
						metadataExtended : metadataExtended,
						relatedNfpMint : oldestNfp.mint,
						relatedNfpAccount : oldestNfp.tokenAccount,
						parentNfpMint : oldestNfp.extendedData.parentNfp,
						parentNfpAccount : parentNfpAccount,
						parentWallet : parentWallet,
						grandParentNfpMint : oldestNfp.extendedData.grandParentNfp,
						grandParentNfpAccount : grandParentNfpAccount,
						grandParentWallet : grandParentWallet,
						scobyWallet : chcPoolData.scobyWallet,
						creatorNftAccount : creatorNftAccount,
						creatorWallet : creatorWallet,
						creatorScoutNftAccount : creatorScoutNftAccount,
						creatorScoutWallet : creatorScoutWallet,
						tokenProgram : TOKEN_PROGRAM_ID,
						tokenMetadataProgram : TOKEN_METADATA_PROGRAM_ID,
						systemProgram : SystemProgram.programId,
						rent : SYSVAR_RENT_PUBKEY,					
					}
				}))
			}
			// await sendTransaction(tx,[])
			await sendTransaction(transaction,signers)
			setAlertState({open: true, message:"Congratulations! Succeeded!",severity:'success'})
			await getPoolData()
		}catch(err){
			console.log(err)
			setAlertState({open: true, message:"Failed! Please try again!",severity:'error'})
		}
	}

	async function sendTransaction(transaction : Transaction, signers : Keypair[]) {
		transaction.feePayer = wallet.publicKey
		transaction.recentBlockhash = (await conn.getRecentBlockhash('max')).blockhash;
		await transaction.setSigners(wallet.publicKey,...signers.map(s => s.publicKey));
		if(signers.length != 0) await transaction.partialSign(...signers)
		const signedTransaction = await wallet.signTransaction(transaction);
		let hash = await conn.sendRawTransaction(await signedTransaction.serialize());
		await conn.confirmTransaction(hash);
		return hash
	}

	return <>
		<main className='content'>
			<div className="card">
			{
				poolData != null && 
				<h6 className="card-title">Mint CHC: {poolData.countMinting+ " CHCs were minted"}</h6>
			}
				<form className="form">
					{
						(wallet && wallet.connected) &&
						<button type="button" disabled={isProcessing==true} className="form-btn" style={{"justifyContent" : "center"}} onClick={async ()=>{
							setIsProcessing(true)
							setAlertState({open: true, message:"Processing transaction",severity: "warning"})
							await mint()
							setIsProcessing(false)
						}}>
							{ isProcessing==true ? "Processing..." :"Mint" }
						</button>
					}
					<WalletConnect/>
				</form>
			</div>
			<Grid container spacing={1}>
			{
				holdingNfts.map((item, idx)=>{
					return <Grid item xs={2}>
						<Card key={idx} sx={{minWidth : 300}}>
							{/* <CardMedia component="img" height="200" image={item.offChainData.image} alt="green iguana"/> */}
							<CardContent>
							<Typography gutterBottom variant="h6" component="div">
								{item.data.name}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"mint : " + item.mint}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"parent : " + item.extendedData.parentNfp}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"grandparent : "+ item.extendedData.grandParentNfp}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"Followers : " + item.extendedData.childrenCount}
								</Typography>
							</CardContent>
						</Card>
					</Grid>
				})
			}
			</Grid>
			<Snackbar
        open={alertState.open}
        autoHideDuration={alertState.severity != 'warning' ? 6000 : 1000000}
        onClose={() => setAlertState({ ...alertState, open: false })}
      >
        <Alert
        	iconMapping={{warning : <CircularProgress size={24}/>}}
          onClose={() => setAlertState({ ...alertState, open: false })}
          severity={alertState.severity}
        >
          {alertState.message}
        </Alert>
      </Snackbar>
		</main>
	</>
}